package IPL;

public class Constants {
    static final int ID_COLUMN = 0;
    static final int SEASON_COLUMN = 1;
    static final int WINNER_COLUMN = 10;

    static final int MATCH_ID_INDEX = 0;
    static final int BOWLING_TEAM_COLUMN = 3;
    static final int BOWLER_COLUMN = 8;
    static final int WIDE_RUNS_COLUMN = 10;
    static final int BYE_RUNS_COLUMN = 11;
    static final int LEG_BYE_RUNS_COLUMN = 12;
    static final int NO_BALL_RUNS_COLUMN = 13;
    static final int PENALTY_RUNS_COLUMN = 14;
    static final int EXTRA_RUNS_COLUMN = 16;
    static final int TOTAL_RUNS_COLUMN = 17;
    static final int ONE_MATCH = 1;
    static final int ONE_WIN = 1;
    static final int ONE_BALL = 1;
    static final double OVER = 6d;

    static final int INITIAL_VALUE = 0;

    static final int SEASON_2015 = 2015;
    static final int SEASON_2016 = 2016;

    static final String REGEX = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";

    static final String REPLACE_IT = ",";
    static final String REPLACE_BY = ", ";

    static final String MATCH_FILE_PATH = "matches.csv";
    static final String DELIVERY_FILE_PATH = "deliveries.csv";
}
