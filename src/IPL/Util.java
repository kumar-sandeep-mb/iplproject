package IPL;

import java.util.ArrayList;
import java.util.HashMap;

import static IPL.Constants.*;

public class Util {
    static HashMap<Integer, Integer> findMatchPlayedPerYear(ArrayList<Match> matchList) {
        HashMap<Integer, Integer> matchPlayedPerYear = new HashMap<>();
        int totalMatches = matchList.size();
        for (int i = INITIAL_VALUE; i < totalMatches; i++) {
            if (matchPlayedPerYear.containsKey(matchList.get(i).getSeason())) {
                matchPlayedPerYear.put(matchList.get(i).getSeason(), matchPlayedPerYear.get(matchList.get(i).getSeason()) + ONE_MATCH);
            } else {
                matchPlayedPerYear.put(matchList.get(i).getSeason(), ONE_MATCH);
            }
        }
        return matchPlayedPerYear;
    }

    static HashMap<String, Integer> findMatchWonByEachTeam(ArrayList<Match> matchList) {
        HashMap<String, Integer> matchPlayedPerYear = new HashMap<>();
        int totalMatches = matchList.size();
        for (int i = INITIAL_VALUE; i < totalMatches; i++) {
            if (matchPlayedPerYear.containsKey(matchList.get(i).getWinner())) {
                matchPlayedPerYear.put(matchList.get(i).getWinner(), matchPlayedPerYear.get(matchList.get(i).getWinner()) + ONE_WIN);
            } else {
                matchPlayedPerYear.put(matchList.get(i).getWinner(), ONE_WIN);
            }
        }
        if (matchPlayedPerYear.containsKey("")) {
            matchPlayedPerYear.put("No Result", matchPlayedPerYear.get(""));
            matchPlayedPerYear.remove("");
        }
        return matchPlayedPerYear;
    }

    static HashMap<String, Integer> findExtraRunsConcededPerTeam(ArrayList<Match> matchList, ArrayList<Delivery> deliveryList) {
        HashMap<String, Integer> extraRunsConcededPerTeam = new HashMap<>();
        ArrayList<Integer> matchId = new ArrayList<>();
        for (int i = INITIAL_VALUE; i < matchList.size(); i++) {
            if (matchList.get(i).getSeason() == SEASON_2016) {
                matchId.add(matchList.get(i).getId());
            }
        }
        for (int i = INITIAL_VALUE; i < deliveryList.size(); i++) {
            if (matchId.contains(deliveryList.get(i).getMatchId())) {
                if (extraRunsConcededPerTeam.containsKey(deliveryList.get(i).getBowlingTeam())) {
                    extraRunsConcededPerTeam.put(deliveryList.get(i).getBowlingTeam(),
                            extraRunsConcededPerTeam.get(deliveryList.get(i).getBowlingTeam()) + deliveryList.get(i).getExtraRuns());
                } else {
                    extraRunsConcededPerTeam.put(deliveryList.get(i).getBowlingTeam(), deliveryList.get(i).getExtraRuns());
                }
            }
        }
        return extraRunsConcededPerTeam;
    }

    static HashMap<String, Double> findMostEconomicalBowlerOfSeason(ArrayList<Match> matchList, ArrayList<Delivery> deliveryList) {
        HashMap<String, Integer> ballsBowledPerBowler = new HashMap<>();
        HashMap<String, Integer> runsConcededPerBowler = new HashMap<>();
        ArrayList<Integer> matchId = new ArrayList<>();
        for (int i = INITIAL_VALUE; i < matchList.size(); i++) {
            if (matchList.get(i).getSeason() == SEASON_2015) {
                matchId.add(matchList.get(i).getId());
            }
        }
        for (int i = INITIAL_VALUE; i < deliveryList.size(); i++) {
            if (matchId.contains(deliveryList.get(i).getMatchId())) {
                if ((deliveryList.get(i).getWideRuns() + deliveryList.get(i).getNoBallRuns()) == 0) {
                    if (ballsBowledPerBowler.containsKey(deliveryList.get(i).getBowler())) {
                        ballsBowledPerBowler.put(deliveryList.get(i).getBowler(), ballsBowledPerBowler.get(deliveryList.get(i).getBowler()) + ONE_BALL);
                    } else {
                        ballsBowledPerBowler.put(deliveryList.get(i).getBowler(), ONE_BALL);
                    }
                }
                if (runsConcededPerBowler.containsKey((deliveryList.get(i)).getBowler())) {
                    runsConcededPerBowler.put(deliveryList.get(i).getBowler(),
                            runsConcededPerBowler.get(deliveryList.get(i).getBowler()) + deliveryList.get(i).getTotalRuns() -
                                    deliveryList.get(i).getByeRuns() - deliveryList.get(i).getLegByeRuns() - deliveryList.get(i).getPenaltyRuns());
                } else {
                    runsConcededPerBowler.put(deliveryList.get(i).getBowler(), deliveryList.get(i).getTotalRuns() -
                            deliveryList.get(i).getByeRuns() - deliveryList.get(i).getLegByeRuns() - deliveryList.get(i).getPenaltyRuns());
                }
            }
        }
        double minAverage = 36d;
        String bowlerName = " ";
        for (String name : ballsBowledPerBowler.keySet()) {
            if (minAverage > ((double) runsConcededPerBowler.get(name) / (double) ballsBowledPerBowler.get(name)) * OVER) {
                minAverage = ((double) runsConcededPerBowler.get(name) / (double) ballsBowledPerBowler.get(name)) * OVER;
                bowlerName = name;
            }
        }
        HashMap<String, Double> mostEconomicalBowler = new HashMap<>();
        mostEconomicalBowler.put(bowlerName, minAverage);
        return mostEconomicalBowler;
    }
}
