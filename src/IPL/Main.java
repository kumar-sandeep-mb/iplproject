package IPL;

import java.util.ArrayList;
import java.util.HashMap;

import static IPL.Resource.*;

public class Main {

    public static void main(String[] args) {
        ArrayList<Match> matchList = fetchMatchData();
        ArrayList<Delivery> deliveryList = fetchDeliveryData();
        HashMap<Integer, Integer> matchPlayedPerYear = Util.findMatchPlayedPerYear(matchList);
        HashMap<String, Integer> matchWonByEachTeam = Util.findMatchWonByEachTeam(matchList);
        HashMap<String, Integer> mostExtraRunsConcededByTeamIn2016 = Util.findExtraRunsConcededPerTeam(matchList, deliveryList);
        HashMap<String, Double> mostEconomicalBowlerOf2015 = Util.findMostEconomicalBowlerOfSeason(matchList, deliveryList);
        printResult(matchPlayedPerYear);
        printResult(matchWonByEachTeam);
        printResult(mostExtraRunsConcededByTeamIn2016);
        printResult(mostEconomicalBowlerOf2015);
    }
}
