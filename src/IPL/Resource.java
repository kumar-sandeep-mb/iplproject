package IPL;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

import static IPL.Constants.*;

public class Resource {

    static BufferedReader fetchFile(String filePath) {
        FileReader fileReader;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader(filePath);
            bufferedReader = new BufferedReader(fileReader);
        } catch (Exception e) {
            showErrorMessage(e);
        }
        return bufferedReader;
    }

    static ArrayList<Match> fetchMatchData() {
        ArrayList<Match> matchList = new ArrayList<>();
        try {
            BufferedReader matchBufferedReader = fetchFile(MATCH_FILE_PATH);
            String matchRow = matchBufferedReader.readLine();
            while ((matchRow = matchBufferedReader.readLine()) != null) {
                matchRow = matchRow.replaceAll(REPLACE_IT, REPLACE_BY);
                String[] matchData = matchRow.split(REGEX);
                Match match = new Match();
                match.setId(Integer.parseInt(matchData[ID_COLUMN].trim()));
                match.setSeason(Integer.parseInt(matchData[SEASON_COLUMN].trim()));
                match.setWinner(matchData[WINNER_COLUMN].trim());
                matchList.add(match);
            }
        } catch (Exception e) {
            showErrorMessage(e);
        }
        return matchList;
    }

    static ArrayList<Delivery> fetchDeliveryData() {
        ArrayList<Delivery> deliveryList = new ArrayList<>();
        try {
            BufferedReader deliveryBufferedReader = fetchFile(DELIVERY_FILE_PATH);
            String deliveryRow = deliveryBufferedReader.readLine();
            while ((deliveryRow = deliveryBufferedReader.readLine()) != null) {
                deliveryRow = deliveryRow.replaceAll(REPLACE_IT, REPLACE_BY);
                String[] deliveryData = deliveryRow.split(REGEX);
                Delivery delivery = new Delivery();
                delivery.setMatchId(Integer.parseInt(deliveryData[MATCH_ID_INDEX].trim()));
                delivery.setBowlingTeam(deliveryData[BOWLING_TEAM_COLUMN].trim());
                delivery.setBowler(deliveryData[BOWLER_COLUMN].trim());
                delivery.setWideRuns(Integer.parseInt(deliveryData[WIDE_RUNS_COLUMN].trim()));
                delivery.setByeRuns(Integer.parseInt(deliveryData[BYE_RUNS_COLUMN].trim()));
                delivery.setLegByeRuns(Integer.parseInt(deliveryData[LEG_BYE_RUNS_COLUMN].trim()));
                delivery.setNoBallRuns(Integer.parseInt(deliveryData[NO_BALL_RUNS_COLUMN].trim()));
                delivery.setPenaltyRuns(Integer.parseInt(deliveryData[PENALTY_RUNS_COLUMN].trim()));
                delivery.setExtraRuns(Integer.parseInt(deliveryData[EXTRA_RUNS_COLUMN].trim()));
                delivery.setTotalRuns(Integer.parseInt(deliveryData[TOTAL_RUNS_COLUMN].trim()));
                deliveryList.add(delivery);
            }
        } catch (Exception e) {
            showErrorMessage(e);
        }
        return deliveryList;
    }

    static void printResult(HashMap<?, ?> hashMap) {
        System.out.println(hashMap);
    }

    static void showErrorMessage(Exception e) {
        System.out.println(e);
    }
}
